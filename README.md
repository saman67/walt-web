# Walt Web App


#### Follow the following steps to publish the web app on a Ubuntu 16+ machine with preinstalled apache2 server:

1. Prepare the website directory:  
    $ cd /var/www/  
    $ mkdir walt-app  
    $ cd walt-app  

2. Clone the repository to the server:
    $ git clone https://bitbucket.org/saman67/walt-web.git


3. Create a apache2 config file for the website:
    $ sudo vim /etc/apache2/sites-available/walt-web.conf


4. Copy this content to the file and save the file:  
    <VirtualHost *:80>  
    	ServerName domain.com  
    	DocumentRoot /var/www/walt-web/dist  
    
    	# Redirect for angular routes  
    	RewriteEngine on  
    	RewriteCond %{REQUEST_FILENAME} -s [OR]  
    	RewriteCond %{REQUEST_FILENAME} -l [OR]  
    	RewriteCond %{REQUEST_FILENAME} -d  
    	RewriteRule ^.*$ - [NC,L]  
    	RewriteRule ^(.*) /index.html [NC,L]  
    </VirtualHost>  


5. Enable Mode Rewrite:  
    $ sudo a2enmod rewrite  


6. Add website config file to the apache2 server:  
    $ a2ensite walt-web.conf  


7. Restart the server:  
    $ service apache2 restart  


const baseUrl = "http://198.74.51.221/api/v1/";

export const environment = {
  production: true,
  navigation: {
    afterLogin: '/interviews',
    afterSignup: '/interviews',
  },
  api: {
    tokenUrl: baseUrl + "token",
    signupUrl: baseUrl + "users",
    interviewsUrl: baseUrl + "interview",
    interviewMessagesUrl: baseUrl + "interview/{interview_id}/messages",
    interviewResultUrl: baseUrl + "interview/{interview_id}/result",
    interviewResourcesUrl: baseUrl + "interview/{interview_id}/resources/{skill}"
  }
};

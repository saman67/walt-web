// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const baseUrl = "http://198.74.51.221:5010/api/v1/";

export const environment = {
  production: false,
  navigation: {
    afterLogin: '/interviews',
    afterSignup: '/interviews',
  },
  api: {
    tokenUrl: baseUrl + "token",
    signupUrl: baseUrl + "users",
    interviewsUrl: baseUrl + "interview",
    interviewMessagesUrl: baseUrl + "interview/{interview_id}/messages",
    interviewResultUrl: baseUrl + "interview/{interview_id}/result",
    interviewResourcesUrl: baseUrl + "interview/{interview_id}/resources/{skill}"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

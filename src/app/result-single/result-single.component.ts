import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from "@angular/router";
import {InterviewService} from "../interview.service";
import {load} from "@angular/core/src/render3/instructions";

@Component({
  selector: 'app-result-single',
  templateUrl: './result-single.component.html',
  styleUrls: ['./result-single.component.css']
})
export class ResultSingleComponent implements OnInit {

  interview_id: number;
  skill: number;
  resources: Object = {
    content: [],
    score: 0
  };

  loading: boolean = true;

  constructor(private interviewService: InterviewService,
              private activeRouter: ActivatedRoute, public router: Router)
  {
    activeRouter.queryParams.subscribe(params => {
      this.interview_id = +params['id'];
      this.skill = params['skill'];
    });
  }

  ngOnInit() {
    this.loadContent();
  }

  loadContent() {
    this.loading = true;
    this.interviewService.getInterviewResources(this.interview_id, this.skill)
      .subscribe(response => {
        this.resources=response;

        this.loading = false;
      }, err => {
        console.error(err.message);
        this.loading = false;
      });
  }

  getScoreStyle(side) {
    var style = {};
    var score = this.resources['score'] * 100;
    var degree = 0;

    if (side == 'left') {
      if (score >= 50)
        style["transform"] = "rotate(180deg)";
      else {
        degree = 180 + (180 / 50 * (score - 50));
        style["transform"] = "rotate(" + degree + "deg)";
      }
    }
    else if (side == 'right') {
      if (score <= 50)
        style["transform"] = "rotate(0deg)";
      else {
        degree = 180 / 50 * score;
        style["transform"] = "rotate(" + degree + "deg)";
      }
    }

    style["border-color"] = this.resources['color'];

    return style;
  }


  goToInterviewResult() {
    this.router.navigate(['result/intro'], {queryParams: {id: this.interview_id}});
  }
}

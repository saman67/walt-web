import { Component, OnInit } from '@angular/core';
import {AuthService} from "../auth.service";
import {Router} from "@angular/router";
import {environment} from "../../environments/environment";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  username:string;
  password:string;
  fullname:string;
  info:string;

  meessage:string;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    if(this.authService.getToken())
      this.router.navigate([environment.navigation.afterSignup]);
  }

  onSignup()
  {
    this.meessage = "";
    this.authService.signup(this.username, this.password, this.fullname, this.info)
      .subscribe(response => {

        this.authService.login(this.username, this.password)
          .subscribe(
            response => {
              if(response["token"])
                this.authService.afterLoginAction(response, environment.navigation.afterSignup);
              else
                console.error("Login response has no token!");

            }, err => {
              alert("Error: login after signup failed!")
            });

      }, err => {
        if(err.status == 422)
          this.meessage = "User is already taken!";
        else if(err.error && err.error.errorMessage)
          this.meessage = err.error.errorMessage;
        else
          this.meessage = "Something went wrong.";
      });
  }

}

import { Input, Component, OnInit } from '@angular/core';
import {AuthService} from "../auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  @Input() interview_id: number;
  @Input() interview_status;

  constructor(private authService: AuthService, public router: Router ) { }

  ngOnInit() { }

  backToHome() {
    this.router.navigate(['/']);
  }

  goToResult()
  {
    this.router.navigate(['result/intro'], {queryParams: {id: this.interview_id}});
  }

  openInterview(id)
  {
    this.router.navigate(['interview/'], {queryParams: {id}});
  }

  logOut() {
    this.authService.logOut();
  }
}

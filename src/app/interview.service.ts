import { Injectable } from '@angular/core';
import { Interview } from "./models/interview";
import { environment } from "../environments/environment";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { catchError, map } from "rxjs/internal/operators";
import { Message } from "./models/message";


@Injectable({
  providedIn: 'root'
})
export class InterviewService
{
  constructor(private http: HttpClient) { }

  getAllInterviews() {
    return this.http.get<Interview[]>(environment.api.interviewsUrl)
      .pipe(map(data => data));
  }

  getOneInterviews(interview_id) {
    return this.http.get<Interview>(environment.api.interviewsUrl + '/' + interview_id);
  }

  createInterview() {
    return this.http.post(environment.api.interviewsUrl, {}, {});
  }

  deleteInterview(interview_id) {
    return this.http.delete(environment.api.interviewsUrl + '/' + interview_id);
  }

  getInterviewMessages(interview_id)
  {
    var url = environment.api.interviewMessagesUrl.replace('{interview_id}', interview_id);
    return this.http.get<Message[]>(url)
      .pipe(map(data => data));
  }

  sendMessages(interview_id, question_id, message) {
    var url = environment.api.interviewMessagesUrl.replace('{interview_id}', interview_id);
    return this.http.post(url, {
      text: message,
      question_id: question_id
    }, {});
  }

  getInterviewResult(interview_id) {
    var url = environment.api.interviewResultUrl.replace('{interview_id}', interview_id);
    return this.http.get(url);
  }

  getInterviewResources(interview_id, skill) {
    var url = environment.api.interviewResourcesUrl;
    url = url.replace('{interview_id}', interview_id);
    url = url.replace('{skill}', skill);

    return this.http.get(url);
  }
}

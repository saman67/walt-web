import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { InterviewsComponent } from './interviews/interviews.component';
import { InterviewComponent } from './interview/interview.component';
import { AuthInterceptor } from "./auth.service";
import { AppRoutingModule } from './app-routing.module';
import { LoadingComponent } from './loading/loading.component';
import { NavigationComponent } from './navigation/navigation.component';
import { ResultIntroComponent } from './result-intro/result-intro.component';
import { ResultSingleComponent } from './result-single/result-single.component';
import { ResultFullComponent } from './result-full/result-full.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    InterviewsComponent,
    InterviewComponent,
    LoadingComponent,
    NavigationComponent,
    ResultIntroComponent,
    ResultSingleComponent,
    ResultFullComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,

  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import {Component, Renderer2} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';


  constructor(private router: Router, private renderer: Renderer2) {
    // router.events.subscribe((val) => {
    //
    //   // Add route class to body to use shared css
    //   if(val['url'] && val["navigationTrigger"] == "imperative")
    //   {
    //     var classes = document.body.getAttribute('class');
    //     var newClasses = val['url'].substring(1).replace(/\?.*/g, '').replace('/', '-');
    //
    //     if(classes != newClasses) {
    //       this.renderer.removeClass(document.body, classes);
    //       if(newClasses)
    //         this.renderer.addClass(document.body, newClasses);
    //     }
    //   }
    //
    // });
  }

}

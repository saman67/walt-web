import { NgModule } from '@angular/core';
import {Routes, RouterModule} from "@angular/router";
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { InterviewComponent } from './interview/interview.component';
import { InterviewsComponent } from './interviews/interviews.component';
import { ResultIntroComponent } from './result-intro/result-intro.component';
import { ResultFullComponent } from './result-full/result-full.component';
import { ResultSingleComponent } from './result-single/result-single.component';


const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'interview', component: InterviewComponent  },
  { path: 'interviews', component: InterviewsComponent },
  { path: 'result/intro', component: ResultIntroComponent },
  { path: 'result/full', component: ResultFullComponent },
  { path: 'result/single', component: ResultSingleComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule { }

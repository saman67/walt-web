import { Component, OnInit } from '@angular/core';
import {AuthService} from "../auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isHidden: boolean = false;

  constructor(private authService: AuthService, public router: Router) {
    router.events.subscribe((val) => {
      // Hide header if on the login or sign up page
      // if (val["navigationTrigger"] == "imperative")
        if(val['url'] == '/login' || val['url'] == '/signup')
          this.isHidden = true;
        else
          this.isHidden = false;
    });
  }


  ngOnInit() { }


  logOut() {
    this.authService.logOut();
  }
}

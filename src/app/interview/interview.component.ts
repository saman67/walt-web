import { Component, OnInit } from '@angular/core';
import { Message } from '../models/message'
import {InterviewService} from "../interview.service";
import { Router, ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-interview',
  templateUrl: './interview.component.html',
  styleUrls: ['./interview.component.css']
})
export class InterviewComponent implements OnInit {

  interview_id: number;

  messages: Message[];

  answer_box: string = '';

  interview_status: string;

  loading: boolean = true;

  limits = {
    answerMinWords: 0,
    answerMinCharacters: 0,
    answerMaxCharacters: 1000,
  };

  constructor(private interviewService: InterviewService,
              private router: Router,
              private activeRouter: ActivatedRoute )
  {
    activeRouter.queryParams.subscribe(params => {
      this.interview_id = +params['id'];
    });
  }

  ngOnInit()
  {
    this.getInterviewInfo();
    this.loadMessages(false);
  }

  getInterviewInfo()
  {
    this.interviewService.getOneInterviews(this.interview_id)
      .subscribe(interview => {
        this.interview_status = interview["status"][0];
        this.limits.answerMinWords = interview["answerMinWords"];
        this.limits.answerMinCharacters = interview["answerMinCharacters"];
        this.limits.answerMaxCharacters = interview["answerMaxCharacters"];
      });
  }

  loadMessages(goToBottom: boolean)
  {
    this.interviewService.getInterviewMessages(this.interview_id)
      .subscribe(messages => {
        this.messages = messages;
        this.loading = false;

        if(goToBottom)
          this.scrollToBottom()
      });

  }

  scrollToBottom() {
    setTimeout(()=>{
      window.scrollTo(0, document.body.scrollHeight);
    }, 200);
  }


  giveAnswer()
  {
    // Don't let the user answer if the questions are not loaded yet!
    if(this.messages.length < 2)
      return;

    var words = this.countWords(this.answer_box);
    if(words < this.limits.answerMinWords || this.answer_box.length < this.limits.answerMinCharacters)
      return alert('Please write at least ' + this.limits.answerMinWords +
        ' words. You need ' + (this.limits.answerMinWords-words) + ' more words.');

    if(words > this.limits.answerMaxCharacters)
      return alert('Wow! Your answer with ' + this.answer_box.length +
        ' characters is too long. Please write an answer up to ' +
        this.limits.answerMaxCharacters + ' characters.');

    var last_message = this.messages[this.messages.length-1];
    if(last_message.type != 'Q')
      return;

    if(!last_message.question_id)
      return;

    this.interviewService.sendMessages(this.interview_id, last_message.question_id, this.answer_box)
      .subscribe(response => {
        this.loadMessages(true);
        this.interview_status = response['interview_status'];
        this.answer_box = "";



        if(this.interview_status == 'C')
          this.goToResult();

      }, err => {
        // Not a genuine answer
        if(err.status == 403)
          alert(err.error.errorMessage);
        else
          console.log("Error: " + err.error.errorMessage);
      });
  }

  countWords(str: string) {
    if(str == null || str.trim() == '')
      return 0;
    return str.trim().split(/\s+/).length;
  }

  goToResult()
  {
    this.router.navigate(['result/intro'], {queryParams: {id: this.interview_id}});
  }
}

export class Message
{
  id: number;
  user_id: number;
  interview_id: number;
  type: string;
  question_id: number;
  text: string;
  created: string;
}

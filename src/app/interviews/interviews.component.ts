import { Component, OnInit } from '@angular/core';
import {Interview} from "../models/interview";
import {InterviewService} from "../interview.service";
import {Router} from "@angular/router";
import {AuthService} from "../auth.service";

@Component({
  selector: 'app-interviews',
  templateUrl: './interviews.component.html',
  styleUrls: ['./interviews.component.css']
})
export class InterviewsComponent implements OnInit {

  interviews:Interview[];

  constructor(private interviewService: InterviewService,
              private authService: AuthService,
              private router: Router) {}


  ngOnInit() {
    this.interviewService.getAllInterviews().subscribe(interviews => (this.interviews = interviews))
  }


  onNewInterview()
  {
    this.interviewService.createInterview()
      .subscribe(response => {
        var id = response['interview_id'];
        this.openInterview(id);
      }, err => {
        console.log("Error: " + err.error.errorMessage);
      });
  }


  deleteInterview(interview_id: number)
  {
    this.interviewService.deleteInterview(interview_id)
      .subscribe(response => {
        this.interviewService.getAllInterviews().subscribe(interviews => (this.interviews = interviews))
      });
  }

  openInterview(id)
  {
    this.router.navigate(['interview/'], {queryParams: {id}});
  }


  goToResult(interview_id)
  {
    this.router.navigate(['result/intro'], {queryParams: {id: interview_id}});
  }


  logout() {
    this.authService.logOut();
  }
}

import { Component, OnInit } from '@angular/core';
import { InterviewService } from "../interview.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-result-full',
  templateUrl: './result-full.component.html',
  styleUrls: ['./result-full.component.css']
})
export class ResultFullComponent implements OnInit {

  result: Object;
  interview_id: number;

  loading: boolean = true;

  constructor(private interviewService: InterviewService,
              private activeRouter: ActivatedRoute, public router: Router)
  {
    activeRouter.queryParams.subscribe(params => {
      this.interview_id = +params['id'];
    });
  }

  ngOnInit() {
    this.loadResult();
  }

  loadResult() {

    this.loading = true;

    this.interviewService.getInterviewResult(this.interview_id)
      .subscribe(response => {
        this.result = response;
        this.loading = false;
      }, err => {
        console.error(err.error.errorMessage);
        this.loading = false;
      });
  }


  extractHostname(url) {
    var hostname;
    //find & remove protocol (http, ftp, etc.) and get hostname

    if (url.indexOf("://") > -1) {
      hostname = url.split('/')[2];
    }
    else {
      hostname = url.split('/')[0];
    }

    //find & remove port number
    hostname = hostname.split(':')[0];
    //find & remove "?"
    hostname = hostname.split('?')[0];

    return hostname;
  }

  extractRootDomain(url) {
    var domain = this.extractHostname(url),
      splitArr = domain.split('.'),
      arrLen = splitArr.length;

    //extracting the root domain here
    //if there is a subdomain
    if (arrLen > 2) {
      domain = splitArr[arrLen - 2] + '.' + splitArr[arrLen - 1];
      //check to see if it's using a Country Code Top Level Domain (ccTLD) (i.e. ".me.uk")
      if (splitArr[arrLen - 2].length == 2 && splitArr[arrLen - 1].length == 2) {
        //this is using a ccTLD
        domain = splitArr[arrLen - 3] + '.' + domain;
      }
    }
    return domain;
  }

  goToSingleResult(skill) {

    var skill_id = skill.replace(new RegExp(' ', 'g'), '_').toLowerCase();

    this.router.navigate(['result/single'], {

      queryParams: {
        id: this.interview_id,
        skill: skill_id
      }});
  }
}

import { Component, OnInit } from '@angular/core';
import { InterviewService } from "../interview.service";
import {ActivatedRoute, Router} from "@angular/router";


@Component({
  selector: 'app-result-intro',
  templateUrl: './result-intro.component.html',
  styleUrls: ['./result-intro.component.css']
})
export class ResultIntroComponent implements OnInit {

  result: Object;
  strong_skills: Array<Object> = [];
  weak_skills: Array<Object> = [];
  interview_id: number;

  loading: boolean = true;

  constructor(private interviewService: InterviewService,
              private activeRouter: ActivatedRoute, public router: Router)
  {
    activeRouter.queryParams.subscribe(params => {
      this.interview_id = +params['id'];
    });
  }

  ngOnInit() {
    this.loadResult();
  }

  loadResult() {
    this.loading = true;
    this.interviewService.getInterviewResult(this.interview_id)
      .subscribe(response => {
        this.result = response;
        this.strong_skills = [];

        for (var i=0; i<6; i++)
        {
          this.strong_skills.push(response[i]);
          this.weak_skills.push(response[i+48]);
        }

        this.loading = false;
      }, err => {
        console.error(err);
        this.loading = false
      });
  }

  public goToSingleResult(skill) {

    var skill_id = skill.replace(new RegExp(' ', 'g'), '_').toLowerCase();

    this.router.navigate(['result/single'], {

      queryParams: {
        id: this.interview_id,
        skill: skill_id
      }});
  }


  public goToFullResult(interview_id) {
    this.router.navigate(['result/full'], {queryParams: {id: interview_id}});
  }
}

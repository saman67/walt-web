import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultIntroComponent } from './result-intro.component';

describe('ResultIntroComponent', () => {
  let component: ResultIntroComponent;
  let fixture: ComponentFixture<ResultIntroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultIntroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultIntroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

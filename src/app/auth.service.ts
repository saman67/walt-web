import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from "@angular/common/http";
import { environment } from '../environments/environment'
import {finalize, tap, catchError} from "rxjs/internal/operators";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService
{
  token: string;
  // cachedRequests: Array<HttpRequest<any>> = [];

  constructor(private http: HttpClient, private router: Router) { }

  login(username:string, password:string) {
    let auth = 'Basic ' + btoa(username + ':' + password);
    const httpOptions = {
      headers: new HttpHeaders ({
        'Content-Type' : 'application/json',
        'Authorization': auth
      })
    };

    return this.http.get(environment.api.tokenUrl, httpOptions);
  }

  signup(username:string, password:string, fullname:string, info:string) {
    const httpOptions = {
      headers: new HttpHeaders ({
        'Content-Type' : 'application/json',
      })
    };

    var data = {
      username: username,
      password: password,
      fullname: fullname,
      info: info
    };

    return this.http.post(
      environment.api.signupUrl,
      data,
      httpOptions
    );
  }

  public getToken() {
    if(!this.token)
      this.token = localStorage.getItem("auth_token");
    return this.token;
  }

  public removeToken() {
    localStorage.removeItem("auth_token");
    this.token = null;
  }

  public logOut() {
    this.removeToken();
    this.router.navigate(['/login']);
  }

  public afterLoginAction(res, route)
  {
    if(res["token"]) {
      this.token = res["token"];
      localStorage.setItem("auth_token", this.token);
      this.router.navigate([route]);
    }
  }

  // private handleError(error: HttpErrorResponse) {
  //   if (error.error instanceof ErrorEvent) {
  //     // A client-side or network error occurred. Handle it accordingly.
  //     console.error('An error occurred:', error.error.message);
  //   } else {
  //     // The backend returned an unsuccessful response code.
  //     // The response body may contain clues as to what went wrong,
  //     console.error(
  //       `Backend returned code ${error.status}, ` +
  //       `body was: ${error.error}`);
  //   }
  //   // return an observable with a user-facing error message
  //   return throwError(
  //     'Something bad happened; please try again later.');
  // };
  //
  // refreshToken(callback) {
  //   AuthService.token = null;
  //
  //   this.http.get(environment.api.tokenUrl).pipe(
  //     catchError(this.handleError)
  //   ).subscribe(( data:Token )=> {
  //     AuthService.token = data.token;
  //
  //     callback(data.token);
  //   });
  // }
  //
  // public collectFailedRequest(request): void {
  //   this.cachedRequests.push(request);
  // }
  //
  // public retryFailedRequests(): void {
  //   this.cachedRequests.pop()
  // }
}




@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(public auth: AuthService, private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler) {

    var authToken = this.auth.getToken();
    var authReq = req;

    // Clone the request and replace the original headers with
    // cloned headers, updated with the authorization.
    if(authToken) {
      authReq = req.clone({setHeaders: {
        //'Authorization': auth,
        'Authorization': 'Basic ' + btoa(authToken+':ss'),
        'Content-Type': 'application/json'
      }});
    }

    return next.handle(authReq)
      .pipe(
        tap(
          event => { },
          (error) => {
            if(error.status == 401) {
              this.auth.removeToken();
              this.router.navigate(['/login']);
            }
          }
        ),
        // Log when response observable either completes or errors
        finalize(() => { })
      );
  }
}

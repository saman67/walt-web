import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service'
import {Router} from "@angular/router";
import {environment} from "../../environments/environment";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username:string;
  password:string;

  meessage:string;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    if(this.authService.getToken())
      this.router.navigate([environment.navigation.afterLogin]);
  }

  onLogin()
  {
    this.meessage = "";
    this.authService.login(this.username, this.password)
      .subscribe(res => {
        if(res["token"])
          this.authService.afterLoginAction(res, environment.navigation.afterLogin);
        else
          console.error("Login response has no token!");
      }, err => {
        if(err.status == 401)
          this.meessage = "Incorrect username and password!";
        else
          this.meessage = "Can not connect to the server!";
      });
  }

}
